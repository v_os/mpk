MPK: Figbox Package Kit
=======================

MPK is the package manager and tool for Figbox/V.OS installations.

It is comprised of 3 different parts:

1. MPKD. This program is designed to be run as a service/daemon and it installs all packages added to the MPK auto directory. 

2. MPK. Main installer program itself (like dpkg)

MPKD:
=====

MPKD isn't complicated, it just scans for a change in the files in the MPK auto directory, installs newly added packages, and then deletes that file

MPK:
====

Arguments:
----------

### `-y` : Do NOT require user input

Bypass any user prompts and continue at each as if user confirmed.

### `-R` : reinitialise.

Creates the directories and files required for MPK on this computer

Note: this will reset current package lists and cache, forgetting those already installed, but will not uninstall packages

ALWAYS prompts user for input before continuing, regardless of `-y` option

### `-i` : Install or `-a` : Add

Install the named package file

### `-r` : Remove

Remove the named installed package

### `-b` : Build

Build a package from the provided folder

### `-m` : Metadata

Print the metadata for the provided package file or installed package name

### `-s` : Check Signature

Check the signature for the provided package file is good


Directories:
------------



Files:
------
