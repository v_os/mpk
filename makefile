
C_SRCS = $(wildcard src/*.c)
SRCS = $(C_SRCS) $(wildcard src/*.h) 

COPT = -Os

all: ./bin/mpk

./bin/mpk: $(SRCS)
	$(CC) $(COPT) $(CFLAGS) -DTIMESTAMP_ISO=$(shell date -r $< -u +'"\"%Y-%m-%d\""') -o $@ $(C_SRCS)

.PHONY: init
init:
	mkdir -p ./bin

clean:
	rm -rf ./bin
