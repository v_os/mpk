/*
Modified BSD License

Copyright (c) 2022-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modified sections of source code used in redistributions covered under
   these licensing terms must be provided freely and free of charge in source
   code form to any party which requests the modifications. Only the modified
   sections need to be made available if the redistribution is included in a
   larger body of work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

const char* ProductInfo = "mpk simple package manager";
const char* BuildDate = TIMESTAMP_ISO;
const char* BuildNumber = "1.0.0";

#define MAX_PACKS 2

int num_files = 0;
char* packages[MAX_PACKS];

int max_reached = 0;

extern int build_pack(const char* folder);
extern int install_pack(const char* package);
extern int remove_pack(const char* package);
extern int print_pack(const char* package, int opt);
extern int init_dirs(const char* root);

const char* root_dir = "/Users/chloe/Documents/Projects/test";

int main(int argc, char** argv)
{
    setbuf(stdout, NULL);

#define MPK_PRINT_META 1
#define MPK_INSTALL    2
#define MPK_REMOVE     4
#define MPK_NOINPUT    8
#define MPK_BUILD      16
#define MPK_CHECKSIG   32

    int opt;
    char* arg;
    int argi;
    int errs;

    argv++;
    for (argc--, argi = 1; (argc > 0); argc--, argi++)
    {
        arg = *argv;

        if (*arg == '-')
        {
            arg++;
            argv++;
            while (*arg && isprint(*arg) && arg != NULL)
            {
                switch (*arg)
                {
                case 'y': /* no user input required */
                    opt |= MPK_NOINPUT;
                    break;

                case 'a': /* add */
                case 'i': /* install */
                    opt |= MPK_INSTALL;
                    break;

                case 'r': /* remove */
                    opt |= MPK_REMOVE;
                    break;

                case 's': /* check signature */
                    opt |= MPK_CHECKSIG;
                    break;

                case 'b': /* build package */
                    opt |= MPK_BUILD;
                    break;

                case 'm': /* print metadata */
                    opt |= MPK_PRINT_META;
                    break;

                case 'R': /* (re)init the directories/files for FPK */
                    return init_dirs(root_dir);

                case 'H': /* Help */
                    printf("usage: mpk [-a,-b,-i,-r,-y,-R,-H,-V] <Package list> \n");
                    return 0;

                case 'V': /* Version */
                    fprintf(stderr, "\n%s, version %s %s\n", ProductInfo, BuildNumber, BuildDate);
                    return 0;

                default:
                    if (*arg && isprint(*arg) && arg != NULL)
                    {
                        printf("Bad flag: %c\n", *arg);
                        return 1;
                    }
                }
                arg++;
            }
        }
        /* non '-' flags are assumed to be the path to list entries in */
        else
        {
            if (num_files < MAX_PACKS)
            {
                packages[num_files++] = arg;
            }
            else if (!max_reached)
            {
                max_reached++;
                fprintf(stderr, "Error: Maximum number of packages reached at argument %i: `%s`\r\n", argi, arg);
                return 1;
            }

            argv++;
            arg++;
        }
    }

    if (num_files < 1)
    {
        fprintf(stderr, "Error: No packages listed");
        return 1;
    }

    for (int i = 0; i < num_files; i++)
    {
        if (opt & MPK_CHECKSIG)
        {
            print_pack(packages[i], 0);
        }
        if (opt & MPK_PRINT_META)
        {
            print_pack(packages[i], 1);
        }

        if (opt & MPK_BUILD)
        {
            build_pack(packages[i]);
        }
        if (opt & MPK_INSTALL)
        {
            install_pack(packages[i]);
        }
        if (opt & MPK_REMOVE)
        {
            remove_pack(packages[i]);
        }
    }

    return 0;
}
